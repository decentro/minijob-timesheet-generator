# Requirements

* NodeJS

# How to use

Open the `app.js` file and edit the variables set on top of the files:

```
var name = "Peter Werner"; // your name
var signatureURL = './unterschrift.png'; // path to signature image; width should be 150px
var maxHoursPerMonth = 20; // maximum hours per month
var month = 2; // month: 0-11
var year = 2019; // year
```

After you set the variables, you can simply create your timesheet with the following command:

```
node app.js
```