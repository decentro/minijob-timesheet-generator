// ====================================================================
// imports
// ====================================================================
var utils = require('./utils');
var Holidays = require('date-holidays');
var xl = require('excel4node');
// ====================================================================

// ====================================================================
// config
// ====================================================================
var name = "Peter Werner"; // your name
var signatureURL = './unterschrift.png'; // path to signature image; width should be 150px
var maxHoursPerMonth = 20; // maximum hours per month
var month = 2; // month: 0-11
var year = 2019; // year
// ====================================================================

// ====================================================================
// functions
// ====================================================================

var hd = new Holidays();
hd.init('DE', 'BY', 'EVANG');

var date1 = new Date(year, month, 1);
var date2 = new Date(year, month+1, 0);

var calcedMonth = calcMonth();
console.log(calcedMonth);

// Create a new instance of a Workbook class
var wb = new xl.Workbook();
// Add Worksheets to the workbook
var ws = wb.addWorksheet('Sheet 1');

// Create a reusable style
var title = wb.createStyle({
  font: {
    color: '#000000',
    size: 14,
    bold: true
  }
});
var bold = wb.createStyle({
  font: {
    color: '#000000',
    size: 11,
    bold: true
  }
});
var styleNormal = wb.createStyle({
  font: {
    color: '#000000',
    size: 11
  }
});

createExcelFile(name, date1, calcedMonth);

// Set value of cell A1 to 100 as a number type styled with paramaters of style
function createExcelFile(name, date, calcedMonth) {

  createHeaderInExcel(name, date, ws);
  createTableHead(ws);
  createDayLines(ws, date);

  // TODO: add randomized data
  addRandomizedData(ws, calcedMonth);

  createFooterData(ws, date);

  wb.write((month+1) + '_' + year + '.xlsx');
}

function addRandomizedData(ws, calcedMonth) {
  calcedMonth.forEach(set => {
    var day = set.date.getDate();
    ws.cell(10 + day, 5)
      .number(set.workHours)
      .style(styleNormal);

    var start = Math.floor(Math.random() * 5) + 9;
    ws.cell(10 + day, 2)
      .string(start + ":00")
      .style(styleNormal);

    var end = set.workHours + start + 1;
    ws.cell(10 + day, 4)
      .string(end + ":00")
      .style(styleNormal);
    ws.cell(10 + day, 3)
      .number(1)
      .style(styleNormal);
  });
}

function createFooterData(ws, date) {
  // curr = new Date(date.getFullYear(), date.getMonth()+1, 1);
  var curr = new Date(Date.now());

  ws.cell(43, 4)
    .string("Summe")
    .style(styleNormal);
  ws.cell(43, 5)
    // .string("20")
    .formula("SUM(E11:E41)")
    .style(styleNormal);
  ws.cell(47, 1)
    .string(curr.getDate() + "." + (curr.getMonth()) + "." + curr.getFullYear())
    .style(styleNormal);
  ws.addImage({
    path: signatureURL,
    type: 'picture',
    position: {
      type: 'oneCellAnchor',
      from: {
        col: 2,
        // colOff: '0.5in',
        row: 46,
        rowOff: 0,
      },
    },
  });
  ws.cell(48, 1)
    .string("Datum")
    .style(styleNormal);
  ws.cell(48, 2)
    .string("Unterschrift Arbeitnehmer")
    .style(styleNormal);
  ws.cell(48, 5)
    .string("Datum")
    .style(styleNormal);
  ws.cell(48, 6)
    .string("Unterschrift Arbeitgeber")
    .style(styleNormal);
}


function createDayLines(ws, date) {
  var daysInMonth = new Date (date.getFullYear(), date.getMonth()+1, 0).getDate();

  for (let i = 0; i < daysInMonth; i++) {
    ws.cell(11+i, 1)
      .number((i+1))
      .style(bold);
  }
}

function createTableHead(ws) {
  ws.cell(9, 1)
    .string('Kalender tag')
    .style(bold);
  ws.cell(9, 2)
    .string('Beginn')
    .style(bold);
  ws.cell(10, 2)
    .string('(Uhrzeit)')
    .style(styleNormal);
  ws.cell(9, 3)
    .string('Pause')
    .style(bold);
  ws.cell(10, 3)
    .string('(Uhrzeit)')
    .style(styleNormal);
  ws.cell(9, 4)
    .string('Ende')
    .style(bold);
  ws.cell(10, 4)
    .string('(Uhrzeit)')
    .style(styleNormal);
  ws.cell(9, 5)
    .string('Dauer')
    .style(bold);
  ws.cell(10, 5)
    .string('(Uhrzeit)')
    .style(styleNormal);
  ws.cell(9, 6)
    .string('Bemerkung')
    .style(bold);
}

function createHeaderInExcel(name, date, ws) {
  monthYear = (date.getMonth() + 1) + " / " + date.getFullYear();

  ws.cell(1, 1)
    .string('Dokumentation der täglichen Arbeitszeit')
    .style(title);
  ws.cell(3, 1)
    .string('Firma:')
    .style(bold);
  ws.cell(3, 5)
    .string('Decentro GmbH')
    .style(styleNormal);
  ws.cell(5, 1)
    .string('Name des Mitarbeiters:')
    .style(bold);
  ws.cell(5, 5)
    .string(name)
    .style(styleNormal);
  ws.cell(7, 1)
    .string('Monat / Jahr:')
    .style(bold);
  ws.cell(7, 5)
    .string(monthYear.toString())
    .style(styleNormal);
}


// =============

function calcMonth() {
  var daysLogged = [];
  while (maxHoursPerMonth > 8) {
    var set = createSet();
    var calc = maxHoursPerMonth - set.workHours;
    maxHoursPerMonth = calc;
    // console.log(set);

    daysLogged.filter(day => {
      if (day.date.getDate() == set.date.getDate()) {
        while (day.date.getDate() == set.date.getDate()) {
          set = createSet();
        }
      }
    });
    daysLogged.push(set);
  }
  daysLogged.push(createSet(maxHoursPerMonth));
  return daysLogged;
}

function createSet(workHours) {
  // var workHours;
  var randomDate = utils.randomDate(date1, date2, hd);

  if (workHours) {
    workHours = workHours;
  } else {
    workHours = utils.randomWorkTime();
  }

  return {workHours: workHours, date: randomDate};
}
// ====================================================================