module.exports = {
  randomDate: function (start, end, hd) {
    var aRandomDate = new Date(+start + Math.random() * (end - start));
    var day = aRandomDate.getDay();
    var isWeekend = (day === 6) || (day === 0)
    if (hd.isHoliday(aRandomDate) || isWeekend) {
      return this.randomDate(start, end, hd);
    } else {
      return aRandomDate;
    }
  },
  randomWorkTime: function () {
    return Math.floor(Math.random() * 4) + 2
  }
};